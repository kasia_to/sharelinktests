$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/FilesPage.feature");
formatter.feature({
  "name": "File browser",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Selecting all items",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "I am in the main folder",
  "keyword": "Given "
});
formatter.match({
  "location": "com.egnyte.steps.FilesPageSteps.openMainFolder()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I select all items",
  "keyword": "When "
});
formatter.match({
  "location": "com.egnyte.steps.FilesPageSteps.selectAllItems()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "all items get selected",
  "keyword": "Then "
});
formatter.match({
  "location": "com.egnyte.steps.FilesPageSteps.checkAllItemsGetSelected()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Sorting files by name",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "I am in the folder with multiple files",
  "keyword": "Given "
});
formatter.match({
  "location": "com.egnyte.steps.FilesPageSteps.openFolderWithMultipleFiles()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "files are sorted by size",
  "keyword": "And "
});
formatter.match({
  "location": "com.egnyte.steps.FilesPageSteps.filesAreSortedBySize()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click sort by name",
  "keyword": "When "
});
formatter.match({
  "location": "com.egnyte.steps.FilesPageSteps.clickSortByName()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "all files are sorted by name",
  "keyword": "Then "
});
formatter.match({
  "location": "com.egnyte.steps.FilesPageSteps.checkAllFilesAreSortedByName()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Downloading single file",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "I am in the folder with multiple files",
  "keyword": "Given "
});
formatter.step({
  "name": "I click file \u003cfilename\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "the clicked file is downloaded",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "filename"
      ]
    },
    {
      "cells": [
        "docx.docx"
      ]
    },
    {
      "cells": [
        "pdf.pdf"
      ]
    },
    {
      "cells": [
        "txt.txt"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Downloading single file",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "I am in the folder with multiple files",
  "keyword": "Given "
});
formatter.match({
  "location": "com.egnyte.steps.FilesPageSteps.openFolderWithMultipleFiles()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click file docx.docx",
  "keyword": "When "
});
formatter.match({
  "location": "com.egnyte.steps.FilesPageSteps.clickFile(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the clicked file is downloaded",
  "keyword": "Then "
});
formatter.match({
  "location": "com.egnyte.steps.FilesPageSteps.checkFileDownloaded()"
});
formatter.result({
  "status": "passed"
});

package com.egnyte.pages;

import com.egnyte.components.FileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;
import java.util.stream.Collectors;

public class FilesPage extends BasePage {

    @FindBy(css = "#content > div > header > div.folderLink-sidebar > div > div > div.folderLink-brand > img")
    private WebElement accessExpiredInfo;

    @FindBy(css = "#content > div > div.folderLink-items-header > div.folderLink-topbar-select > div > div")
    private WebElement selectAllCheckbox;

    @FindBy(id = "folder-items-wrapper")
    private WebElement fileList;

    @FindBy(css = "section.folderLink-items")
    private WebElement fileListSection;

    @FindBy(css = "button[data-mode='gallery']")
    private WebElement galleryButton;

    @FindBy(css = ".folderLink-sort .dropdown-toggle")
    private WebElement sortMenu;

    @FindBy(css = "li[data-sort='name'] a")
    private WebElement sortByName;

    @FindBy(css = "li[data-sort='size'] a")
    private WebElement sortBySize;

    @FindBy(css = "button.folderLink-buttons-download.is-type-folder")
    private WebElement downloadFolderButton;

    private By fileListItemSelector = By.cssSelector("#folder-items-wrapper > ul > li");

    public FilesPage(WebDriver driver) {
        super(driver);
    }

    public boolean isInfoDisplayed() {
        waitForElementVisible(accessExpiredInfo);
        return accessExpiredInfo.isDisplayed();
    }

    public void clickSelectAll() {
        waitForElementVisible(selectAllCheckbox);
        waitForFileListToLoad();
        selectAllCheckbox.click();
        waitForFileListToLoad();
    }

    private void waitForFileListToLoad() {
        wait.until(ExpectedConditions.not(ExpectedConditions.attributeContains(fileListSection, "class", "preloader")));
        wait.until(ExpectedConditions.not(ExpectedConditions.attributeContains(fileList, "class", "preloader")));
        wait.until(ExpectedConditions.elementToBeClickable(fileListSection));
    }

    public List<FileElement> getFiles() {
        waitForFileListToLoad();
        return driver.findElements(fileListItemSelector).stream()
                .map(element -> new FileElement(element, driver))
                .collect(Collectors.toList());
    }

    public void openFolder(String name) throws Exception {
        waitForFileListToLoad();
        findFileByName(name).open();
        waitForFileListToLoad();
    }

    public FileElement findFileByName(String name) throws Exception {
        return getFiles().stream().filter(f -> f.getName().equals(name)).findFirst()
                .orElseThrow(() -> new Exception("File not found"));
    }

    public void switchToGalleryView() {
        galleryButton.click();
    }

    public void sortByName() {
        waitForFileListToLoad();
        sortMenu.click();
        sortByName.click();
        waitForFileListToLoad();
    }

    public void sortBySize() {
        waitForFileListToLoad();
        sortMenu.click();
        sortBySize.click();
        waitForFileListToLoad();
    }

    public void clickDownloadFolder() {
        downloadFolderButton.click();
    }
}

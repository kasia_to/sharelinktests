Feature: File browser

  Scenario: Selecting all items
    Given I am in the main folder
    When I select all items
    Then all items get selected

  #Scenario: Display images in grid view
  #  Given I am in the folder with images
  #  When I click grid view icon
  #  Then images are displayed in grid

  Scenario: Sorting files by name
    Given I am in the folder with multiple files
    And files are sorted by size
    When I click sort by name
    Then all files are sorted by name

  Scenario Outline: Downloading single file
    Given I am in the folder with multiple files
    When I click file <filename>
    Then the clicked file is downloaded

    Examples:
      | filename  |
      | docx.docx |
      | pdf.pdf   |
      | txt.txt   |

  Scenario: Downloading entire folder
    Given I am in the main folder
    When I click Download Folder button
    Then entire folder is downloaded
Feature: password protection

  Scenario: login with correct password
    Given I am on the login page for https://qarecruitment.egnyte.com/fl/lOWeEhHm2m
    When I enter the correct password
    Then I get access to files

  Scenario: login with incorrect password
    Given I am on the login page for https://qarecruitment.egnyte.com/fl/lOWeEhHm2m
    When I enter the incorrect password
    Then Error message is displayed
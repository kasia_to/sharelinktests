package com.egnyte.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {
    @FindBy(id = "password")
    private WebElement passwordInput;

    @FindBy(css = "#password_controls > form > a")
    private WebElement continueButton;

    @FindBy(css = "#password_controls > div")
    private WebElement errorMessage;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void enterPassword(String password) {
        passwordInput.sendKeys(password);
    }

    public void clickContinue() {
        continueButton.click();
    }

    public boolean errorMessageIsDisplayed(){return errorMessage.isDisplayed();}
}

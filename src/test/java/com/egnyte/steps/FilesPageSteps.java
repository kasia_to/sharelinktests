package com.egnyte.steps;

import com.egnyte.DriverProvider;
import com.egnyte.components.FileElement;
import com.egnyte.pages.FilesPage;
import com.egnyte.pages.LoginPage;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class FilesPageSteps extends BaseSteps {
    private FilesPage filesPage;
    private LoginPage loginPage;
    private String fileToDownload;

    @Given("I am in the main folder")
    public void openMainFolder() {
        login();
    }

    @When("I select all items")
    public void selectAllItems() {
        filesPage.clickSelectAll();
    }

    @Then("all items get selected")
    public void checkAllItemsGetSelected() {
        for (FileElement f : filesPage.getFiles()) {
            Assert.assertTrue(f.isSelected());
        }
    }

    @Given("I am in the folder with images")
    public void openFolderWithImages() throws Exception {
        login();
        filesPage.openFolder("DataFolderA");
        filesPage.openFolder("DataFolderB");
        filesPage.openFolder("DataFolderC1");
    }

    @When("I click grid view icon")
    public void clickGridViewIcon() {
        filesPage.switchToGalleryView();
    }

    @Given("I am in the folder with multiple files")
    public void openFolderWithMultipleFiles() throws Exception {
        login();
        filesPage.openFolder("DataFolderA");
        filesPage.openFolder("DataFolderB");
        filesPage.openFolder("DataFolderC2");
    }

    @When("I click sort by name")
    public void clickSortByName() {
        filesPage.sortByName();
    }

    @Then("all files are sorted by name")
    public void checkAllFilesAreSortedByName() {
        List<FileElement> files = filesPage.getFiles();
        Assert.assertEquals(files.stream().sorted(Comparator.comparing(FileElement::getName)).collect(Collectors.toList()), files);
    }

    @When("I click Download Folder button")
    public void clickDownloadFolderButton() {
        filesPage.clickDownloadFolder();
    }

    private void login() {
        driver.get("https://qarecruitment.egnyte.com/fl/lOWeEhHm2m");
        loginPage = new LoginPage(driver);
        loginPage.enterPassword("cvP7cv2Y");
        loginPage.clickContinue();
        filesPage = new FilesPage(driver);
    }

    @Then("entire folder is downloaded")
    public void checkEntireFolderIsDownloaded() {
        checkFileExists("Katarzyna Tomczyk.zip");
    }

    private void checkFileExists(String filename) {
        File file = new File(DriverProvider.getDownloadDirectory(), filename);
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(driver -> file.exists());
    }

    @When("^I click file (.*)$")
    public void clickFile(String file) throws Exception {
        fileToDownload = file;
        filesPage.findFileByName(file).download();
    }

    @Then("the clicked file is downloaded")
    public void checkFileDownloaded() {
        checkFileExists(fileToDownload);
    }

    @After
    public void after() {
        driver.close();
    }

    @And("files are sorted by size")
    public void filesAreSortedBySize() {
        filesPage.sortBySize();
    }
}

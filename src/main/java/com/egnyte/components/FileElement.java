package com.egnyte.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FileElement {
    private static int waitTimeout = 10;
    protected WebDriverWait wait;

    public WebElement element;

    private By nameSelector = By.cssSelector("span.name");
    private By checkboxSelector = By.cssSelector("div.folder-item");
    private By checked = By.cssSelector("div.checkable.select-item.checkbox");
    private By fileName = By.cssSelector("div.file-info > span > span.name");
    private By type = By.className("type folder-item subfolder");

    public FileElement(WebElement element, WebDriver driver) {
        this.element = element;
        wait = new WebDriverWait(driver, waitTimeout);
    }

    public String getName() {
        return element.findElement(nameSelector).getText();
    }

    public boolean isSelected() {
        return element.findElement(checkboxSelector).getAttribute("class").contains("ui-selected");
    }

    public void download() {
        element.findElement(fileName).click();
    }

    public void setSelected(boolean selected) {
        if(isSelected() != selected) {
            element.findElement(checked).click();
        }
    }

    public void open() {
        WebElement title = element.findElement(fileName);
        title.click();
        wait.until(ExpectedConditions.stalenessOf(title));
    }
}

package com.egnyte.steps;

import com.egnyte.pages.FilesPage;
import com.egnyte.pages.LoginPage;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class LoginPageSteps extends BaseSteps {
    private LoginPage loginPage;
    private FilesPage filesPage;

    @Given("^I am on the login page for (.*)")
    public void openLoginPage(String url) {
        driver.get(url);
        loginPage = new LoginPage(driver);
    }

    @When("I enter the correct password")
    public void enterCorrectPassword() {
        loginPage.enterPassword("cvP7cv2Y");
        loginPage.clickContinue();
    }

    @Then("I get access to files")
    public void verifyAccess() {
        filesPage = new FilesPage(driver);
        Assert.assertTrue(filesPage.isInfoDisplayed());

    }

    @When("I enter the incorrect password")
    public void enterInCorrectPassword() {
        loginPage.enterPassword("qwerty");
        loginPage.clickContinue();
    }

    @Then("Error message is displayed")
    public void errorMessageIsDisplayed() {
        Assert.assertTrue(loginPage.errorMessageIsDisplayed());
    }

    @After
    public void after() {
        driver.close();
    }
}
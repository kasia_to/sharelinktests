package com.egnyte.steps;

import com.egnyte.DriverProvider;
import org.openqa.selenium.WebDriver;

public abstract class BaseSteps {
    protected WebDriver driver = new DriverProvider().getDriver();
}
